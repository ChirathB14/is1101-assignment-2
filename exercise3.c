//this program takes two integers and swap them
#include<stdio.h>
int main(){
  int num1,num2;

  printf("Enter value for number 1 :");
  scanf("%d", &num1);
  printf("Enter value for number 2 :");
  scanf("%d", &num2);
  int tmp = num1;
  num1 = num2;
  num2 = tmp;
  printf("Swapped Values: \n Num 1 : %d \n Num 2 : %d\n",num1,num2);

  return 0;

}


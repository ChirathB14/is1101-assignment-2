//this program take 2 numbers and prints the multiplication of two numbers

#include<stdio.h>
int main(){
  float f1,f2;
  printf("Enter number 1 : ");
  scanf("%f", &f1);
  printf("Enter number 2 : ");
  scanf("%f", &f2);
  printf("Multiplication of entered numbers : %.2f\n", f1*f2);
  return 0;

}

//This program takes radius of a disc as an input and output the area of the disc
#include<stdio.h>
const float PI=3.14;
int main(){
  float r;
  printf("Input the radius of the disc : ");
  scanf("%f", &r);
  float area = r*r*PI;
  printf("Area of the disc : %.2f\n", area);
  return 0;
}
